#!/bin/bash
psql postgres -c "DROP DATABASE unicom_test;"
psql postgres -c "CREATE DATABASE unicom_test;"
psql postgres -c "CREATE ROLE unicom_test_user WITH PASSWORD 'unicom_test_password';" 
psql postgres -d unicom_test -c "GRANT ALL ON ALL TABLES IN SCHEMA public TO unicom_test_user;"
