#!/bin/bash

sudo apt-get update
sudo apt-get install \
    python python-pip \
    python3 python3-pip \
    postgresql
pip install pipenv
pipenv --three 
pipenv install -r requirements.txt
pipenv shell
