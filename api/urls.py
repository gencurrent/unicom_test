from django.urls import path, re_path
from django.conf.urls import url
from api import views as api

urlpatterns = [
    path('client_profile_list/', api.ClientProfileList.as_view(), name='client_profile_list_api'),
    path('client_profile/', api.ClientProfileDetail.as_view(), name='client_profile_detail_api_post'),
    path('client_profile/<int:client_profile_id>/$', api.ClientProfileDetail.as_view(), name='client_profile_detail_api'),
    path('offer_list/', api.OfferList.as_view(), name='offer_list_api'),

    # Запросы кредитным организациям
    path('request_to_cc_list/', api.RequestToCCList.as_view(), name='request_to_cc_list_api'), 
    path('request_to_cc/', api.RequestToCCDetail.as_view(), name='request_to_cc_api_post'), 
    path('request_to_cc/<int:request_id>/', api.RequestToCCDetail.as_view(), name='request_to_cc_api'), 
    # path('request_to_cc_list/<int:request_id>/update/', api.RequestToCCList.as_view(), name='request_to_cc_api_update'),  
]