# Django
from django.test.testcases import TestCase
from django.contrib.auth.models import User
from django.urls import reverse

# DRF
from rest_framework.test import APIRequestFactory, force_authenticate

# Other
import pdb
from decimal import Decimal
from django.utils import timezone
from datetime import timedelta

#Custom 
from unicom_test import models as unicom
from profile.models import Profile
from api import views as api_views


class SuperUserTestCase(TestCase):
    fixtures = ['fixtures.json',]

    def print_data(self, **args):
        

    def setUp(self):
        """
        Установка: 
            1. Пользователь - тестовый администратор c username='testadmin2', password='x;8dh_L2&9A'
        """
        
        # Пользователи
        self.user_dict = {
            'admin': User.objects.create_user(is_staff=True, is_superuser=True, username='testadmin2', password='x;8dh_L2&9A'), 
            'partner': User.objects.create_user(is_staff=False, username='testpartner', password='x;8dh_L2&9A'), 
            'cc': User.objects.create_user(is_staff=False, username='testcredit_company', password='x;8dh_L2&9A'), 
        }
        #
        # Кредитная компания (не подвержены тестированию)
        self.credit_company = unicom.CreditCompany.objects.create(name='CC LLC')

        # Профили
        print(self.credit_company)
        self.profile_dict = { 
            'partner': Profile.objects.create(user=self.user_dict['partner'], is_partner=True),
            'cc': Profile.objects.create(user=self.user_dict['cc'], is_partner=False, credit_company=self.credit_company)
        }

        # Оффер
        self.offer = unicom.Offer.objects.create(
            rotation_start=timezone.now()-timedelta(days=12),
            rotation_end=timezone.now()+timedelta(hours=2), 
            name='Тестовое предложение', 
            offer_type=0,
            score_min=Decimal(40.0), 
            score_max=Decimal(80.0),
            credit_organization=self.credit_company
        )
        
        self.factory = APIRequestFactory()

    
    def test_api(self):
        """
        Сценарий: 
            1. POST-запрос на создание анкеты клиента
            2.  
        """

        """
        СУПЕРПОЛЬЗОВАТЕЛЬ
        """
        user = User.objects.get(username='testadmin2')

        # POST
        profile_parameters = {
            'firstname': 'John', 
            'surname': 'Doe', 
            'patronymic': 'Harold',
            'phone': '+79998044653',
            'passport': '4559789456', 
            'score': '23.04',
        }

        response = self.request_client_profile(user, data=profile_parameters, method='post')
        profile_parameters.update({'surname': 'Harward'})
        response = self.request_client_profile(user, data=profile_parameters, method='post')

        response = self.request_client_profile_list(user, )

        # request = self.factory.put('/api/client_list', {'title': 'test_user_name2'})
        
        # Получение данных одного клиента
        response = self.request_client_profile(user, id=2, method='get',)
        
        # Удаление данных о втором профиле
        request = self.request_client_profile(user, 2, method='delete')

        

        """
        ПАРТНЕР
        """
        user = self.user_dict['partner']
        profile_parameters.update({'surname': 'Partner Created'})
        # Создание новой анкеты
        response = self.request_client_profile(user, data=profile_parameters, method='post', comment='0x808070')
        client_profile_id = response.data['id']
        # Не имеет доступа
        response = self.request_client_profile(user, 1, method='delete', comment='0x808078')
        # Создание нового запроса к КО
        request_parameters = {
            'client_profile': client_profile_id,
            'offer': self.offer.id,
            # 'status': 0,
            'cc_to': self.credit_company.pk,
        }
        request = self.request_request_to_cc(user, data=request_parameters, method='post', comment='0x808080')


        """
        СУПЕРПОЛЬЗОВАТЕЛЬ
        """
        # Данные о всех запросах
        self.request_request_to_cc_list(self.user_dict['admin'], method='get', comment='0x808088')


        """
        КРЕДИТНАЯ КОМПАНИЯ
        """
        user = self.user_dict['cc']

        profile_parameters.update({'surname': 'From CC'})
        # Создание новой анкеты (нет доступа для КО)
        response = self.request_client_profile(user, data=profile_parameters, method='post')
        # Получение анкеты (нет доступа для КО)
        response = self.request_client_profile(user, data=profile_parameters, method='get')
        

        # Получение данных о пользовате
        self.request_request_to_cc(user, id=1, method='get')

        r_to_cc = {
            'status': 4
        }
        self.request_request_to_cc(user, id=1, data=r_to_cc, method='put')



    def request_client_profile(self, user, id='', data={}, method='get', comment=''):
        """
        Запрос к профилю клиента с id=id, пользователем user, данными data
        """
        print('\n' + '='*20)
        print(comment)
        path = '/api/client_profile/' + ((str(id) + '/') if id else '')
        print(path)
        print(method.upper())

        view = api_views.ClientProfileDetail.as_view()
        if method.lower() not in ['get', 'post', 'put', 'delete']:
            return False
        # Выхзов метода динамически
        request = getattr(self.factory, method)(path, data)
        # Аутентификация пользователя
        force_authenticate(request, user=user)
        response = view(request, id) if id else view(request)
        print(response.data)
        print('\n' + '='*20)
        return response
    

    def request_client_profile_list(self, user, data={}, method='get', comment=''):
        """
        Запрос на получение списка клиентов 
        """
        print('\n' + '='*20)
        print(comment)
        path = reverse('client_profile_list_api')
        print(path)
        print(method.upper())
        view = api_views.ClientProfileList.as_view()
        if method.lower() == 'get':
            request = self.factory.get(reverse('client_profile_list_api'), data)

        force_authenticate(request, user=user)
        response = view(request)
        print(response.data)
        print('\n' + '='*20)
        return response
    


    def request_request_to_cc(self, user, id='', data={}, method='get', comment=''):
        """
        Запрос к (запросу к кредитной организации) с id=id, пользователем user, данными data
        """
        print('\n' + '='*20)
        print(comment)
        path = '/api/request_to_cc/' + ((str(id) + '/') if id else '')
        print(path)
        print(method.upper())
        print(data)

        view = api_views.RequestToCCDetail.as_view()
        if method.lower() not in ['get', 'post', 'put', 'delete']:
            return False
        # Выхзов метода динамически
        request = getattr(self.factory, method)(path, data)
        # Аутентификация пользователя
        force_authenticate(request, user=user)
        response = view(request, id) if id else view(request)
        print(response.data)
        print('\n' + '='*20)
        return response


    def request_request_to_cc_list(self, user, data={}, method='get', comment=''):
        """
        Запрос к списку (запросов к кредитной организации) с пользователем user, данными data
        """
        print('\n' + '='*20)
        print(comment)
        path = '/api/request_to_cc_list/' 
        print(path)
        print(method.upper())
        print(data)

        view = api_views.RequestToCCList.as_view()
        if method.lower() not in ['get', 'put',]:
            return False
        # Выхзов метода динамически
        request = getattr(self.factory, method)(path, data)
        # Аутентификация пользователя
        force_authenticate(request, user=user)
        response = view(request) if id else view(request)
        print(response.data)
        print('\n' + '='*20)
        return response