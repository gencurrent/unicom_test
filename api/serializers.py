"""
Description:
    The serializers file

Author: 
    Artem Bulatov
"""
from rest_framework import serializers

from unicom_test import models as unicom

class ClientProfileSerializer(serializers.ModelSerializer):
    """
    Client Profile Serializer 
    """

    class Meta: 
        model = unicom.ClientProfile
        exclude = ['partner']


class OfferSerializer(serializers.ModelSerializer):
    """
    Offer Serizlizer
    """

    offer_type = serializers.SerializerMethodField()

    def get_offer_type(self, obj):
        result = [item[1] for item in unicom.Offer.TYPES if item[0] == obj.offer_type]
        if result:
            return result[0]
        else: 
            return ''

    class Meta:
        model = unicom.Offer
        exclude = ['partner']


class RequestListSerializer(serializers.ModelSerializer):
    """
    Request To CC serializer
    """
    class Meta:
        model = unicom.RequestToCС
        fields = ['id', 'status']

class RequestSerializer(serializers.ModelSerializer):
    """
    Request To CC serializer
    """
    class Meta:
        model = unicom.RequestToCС
        fields = '__all__'


class RequestSerializerForCC(serializers.ModelSerializer):
    """
    Request To CC serializer
    """
    class Meta:
        model = unicom.RequestToCС
        fields = ['id', 'status',]