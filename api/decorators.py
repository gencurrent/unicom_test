"""
Description:
    The decorators file

Author: 
    Artem Bulatov
"""

from django.contrib.auth.models import User
from rest_framework.response import Response
from rest_framework import status

import pdb

def get_user(request):
    username = request.data.get('username')
    if not username:
        return None
    try:
        user = User.objects.get(username=username)
    except:
        return None
    return user

def only_superuser(func):
    """
    Доступ только суперпользователю
    """

    def wrapper(*args, **kwargs):
        request = args[1]
        user = request.user
        if (not user.is_staff):
            return Response('The user with username {} is not the superuser'.format(request.data.get('username', '')), status.HTTP_404_NOT_FOUND)
        return func(*args, **kwargs)
    return wrapper

def partner_allowed(func):
    """
    Доступ только суперпользователю
    """
    def wrapper(*args, **kwargs):
        request = args[1]
        user = request.user
        print(request)
        if (not user):
            pdb.set_trace()
            return Response('The user with username {} is not a partner'.format(user.username), status.HTTP_404_NOT_FOUND)
        if user.is_staff:
            return func(*args, **kwargs)
        try: 
            profile = user.profile
            if not profile.is_partner:
                print('The user is not a partner.......')
                raise Exception('Only partners allowed')
        except:
            return Response('The partner does not exist', status.HTTP_404_NOT_FOUND)
        return func(*args, **kwargs)
    return wrapper

def credit_company_allowed(func):
    """
    Доступ только представителей кредитных органиаций 
    """
    def wrapper(*args, **kwargs):
        request = args[1]
        user = request.user
        if (not user):
            return Response('The user with username {} could not been found'.format(request.data.get('username', '')), status.HTTP_404_NOT_FOUND)
        if user.is_staff:
            return func(*args, **kwargs)
        try: 
            cc = user.profile.credit_company
        except Exception as e:
            print(e)
            return Response('The credit company official person does not exist', status.HTTP_404_NOT_FOUND)
        print('profile = {}'.format(user.profile))
        return func(*args, **kwargs)
    return wrapper

