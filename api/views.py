from django.contrib.auth.models import User
from django_filters.rest_framework import DjangoFilterBackend
from django.utils.decorators import method_decorator

from rest_framework.views import APIView, Http404
from rest_framework import generics
from rest_framework import status
from rest_framework.response import Response
from api.serializers import *



from unicom_test import models as unicom
from api import decorators

def form_params(query_params):
    """
    Формирование списка в нормальном виде (по умолчанию Django создает массив значений в value)
    """
    q = {}
    for key, value in query_params.items()  :
        q[key] = value
    return q

class ClientProfileList(APIView):
    """
    Список анкет клиентов
    """
    
    @decorators.only_superuser
    def get(self, request):
        user = request.user
        print('user = {}'.format(user))
        params = request.query_params
        # Получение списка анкет согласно параметрам
        qs = unicom.ClientProfile.objects.filter(partner=request.user, **form_params(params))
        serializer = ClientProfileSerializer(qs, many=True)
        return Response(serializer.data)


class ClientProfileDetail(APIView):
    """
    Анкета клиента
    """
    
    @decorators.partner_allowed
    def get(self, request, client_profile_id):
        # Написать метод для фильтрации на созданные только данным партнером заявки
        try:
            c_profile = unicom.ClientProfile.objects.get(id=client_profile_id, partner=request.user)
        except:
            return Response('The client profile is not found', status.HTTP_404_NOT_FOUND)
        serializer = ClientProfileSerializer(c_profile)
        return Response(serializer.data, status.HTTP_200_OK)

    @decorators.partner_allowed
    def post(self, request, format=None):
        serializer = ClientProfileSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(partner=request.user)
            return Response(serializer.data, status.HTTP_200_OK)
        return Response(serializer.errors, 400)
    
    @decorators.only_superuser
    def put(self, request):
        try:
            c_profile = unicom.ClientProfile.objects.get(id=client_profile_id)
        except:
            return Response('The client profile is not found', status.HTTP_404_NOT_FOUND)
        serializer = ClientProfileSerializer(c_profile, request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status.HTTP_200_OK)
        return Response(serializer.errors, 400)

    @decorators.only_superuser
    def delete(self, request, client_profile_id):
        try:
            c_profile = unicom.ClientProfile.objects.get(id=client_profile_id)
        except:
            return Response('The client profile is not found', status.HTTP_404_NOT_FOUND)
        c_profile.delete()
        return Response('Deleted', status.HTTP_200_OK)



class OfferList(APIView):
    """
    Список предложений 
    """

    @decorators.partner_allowed
    def get(self, request):
        username = request.data.get('username')
        if not username:
            return Http404('The user is not found')
        qs = unicom.Offer.objects.all()
        serializer = OfferSerializer(qs, many=True)

        return Response(serializer.data)


class RequestToCCList(APIView):
    """
    Список предложений
    """
    @decorators.partner_allowed
    def get(self, request):
        user = request.user
        if not user.is_staff:
            try:
                co = user.profile.credit_organization
            except Excpetion as e:
                return Reponse('', 400) 
        qs = unicom.RequestToCС.objects.filter(offer__credit_organization=co)
        
        serializer = RequestSerializer(qs, many=True)

        return Response(serializer.data)


class RequestToCCList(APIView):
    """
    Список предложений
    """
    @decorators.credit_company_allowed
    def get(self, request):
        user = request.user
        if user.is_staff:
            
            class _RequestListSerializer(RequestListSerializer):
                """
                Request To CC serializer
                """
                class Meta:
                    model = unicom.RequestToCС
                    fields = '__all__'
            qs = unicom.RequestToCС.objects.all()
        else: 
            try: 
                _RequestListSerializer = RequestListSerializer
                cc = request.user.profile.credit_company
                qs = unicom.RequestToCС.objects.filter(cc_to=cc.pk)
                if not cc:
                    raise Exception('The user is not the CC')
            except Exception as e:
                print(e)
                raise e    

        serializer = _RequestListSerializer(qs, many=True)

        return Response(serializer.data)


class RequestToCCDetail(APIView):
    """
    Заявка на выдачу займа
    """

    @decorators.credit_company_allowed
    def get(self, request, request_id):
        try:
            conditions = {'pk':request_id}
            if request.user.profile and request.user.profile.credit_company:
                conditions.update({'cc_to_id': request.user.profile.credit_company.pk})
                print(request.user.profile.credit_company.pk)
                print(conditions)
            request_to_cc = unicom.RequestToCС.objects.get(**conditions)
        except Exception as e:
            print(e)
            return Response('The client profile is not found', status.HTTP_404_NOT_FOUND)
        serializer = RequestSerializer(request_to_cc)
        return Response(serializer.data, status.HTTP_200_OK)

    @decorators.partner_allowed
    def post(self, request):
        user = request.user
        fill_data = request.data
        if request.user.profile and request.user.profile.is_partner: 
            if not unicom.ClientProfile.objects.filter(id=fill_data.get('client_profile', None), partner=user).exists():
                return Reponse('The client profile is not found', status=404)
            # fill_data.update({'client_profile': client_profile.pk})

            pass
        serializer = RequestSerializer(data=fill_data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status.HTTP_200_OK)
        return Response(serializer.errors, 400)

    @decorators.credit_company_allowed
    def put(self, request, request_id):
        user = request.user
        try:
            # Работа по условиям фильтрации
            conditions = {'pk':request_id}
            # Если пользователь - КК
            if user.profile and user.profile.credit_company:
                # Только тот запрос, который принадлежит данной КК
                conditions.update({'cc_to': user.profile.credit_company.pk})
            request_to_cc = unicom.RequestToCС.objects.get(**conditions)
        except Exception as e:
            print(e)
            return Response('The request is not found', status.HTTP_404_NOT_FOUND)
        
        # Все возможности для постинга - только для суперпользователя
        if user.is_superuser:
            serializer = RequestSerializer(request_to_cc, data=request.data)
        # Для представителя КК - только обновление статуса
        else:
            serializer = RequestSerializerForCC(request_to_cc, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status.HTTP_200_OK)
        return Response(serializer.errors, 400)

    @decorators.only_superuser
    def delete(self, request, request_id):
        try:
            conditions = {'pk':request_id}
            if request.user.profile and request.user.profile.credit_company:
                conditions.update({'cc_to_id': request.user.profile.credit_company.pk})
                print(request.user.profile.credit_company.pk)
                print(conditions)
            request_to_cc = unicom.RequestToCС.objects.get(**conditions)
        except Exception as e:
            print(e)
            return Response('The request is not found', status.HTTP_404_NOT_FOUND)
        c_profile.delete()
        return Response('Deleted', status.HTTP_200_OK)

