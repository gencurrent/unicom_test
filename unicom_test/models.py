from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from decimal import Decimal


"""
    СОБСТВЕННЫЕ ПОЛЯ
"""
class Timestamp(models.DateTimeField):
    """
    Таймштамп - автоматическое создание времени 
    """
    
    def __init__(self, verbose_name='', null=False, blank=True, default=timezone.now, **kwargs):
        kwargs.update({
            'null': null,
            'blank': blank, 
            'default': default
        })
        super(models.DateTimeField, self).__init__(verbose_name, **kwargs)


class Score(models.DecimalField):
    """
    Скоринговый балл в процентах
    """
    
    def __init__(self, verbose_name=None, name=None, max_digits=5, decimal_places=2, **kwargs):
        # 3 - целое число, 2 - мантисса, значение по умолчанию = 100 
        kwargs['default'] = Decimal(100)
        kwargs['max_digits'] = max_digits
        kwargs['decimal_places'] = decimal_places
        super(models.DecimalField, self).__init__(verbose_name=verbose_name, name=name, **kwargs)



"""
Базовые классы
"""
class BaseTimestamps(models.Model):
    """
    Базовый класс для создания моделей с таймштампами
    """
    created = Timestamp('Создано',)
    edited = Timestamp('Последнее изменение', )

    class Meta:
        abstract = True


"""
Рабочие классы
"""
class Partner(models.Model):
    """
    Партнер компании
    """

    name = models.CharField('Название', max_length=80, null=False, blank=False)
    # user = models.OneToOneField('profile.Profile', null=False, blank=False, on_delete=models.CASCADE, 
    #     verbose_name='Привязанный профиль')
    
    def __str__(self):
        return '{}//{}'.format(self.pk, self.name)
    
    class Meta:
        verbose_name = 'Партнерская организация'
        verbose_name_plural = 'Партнерские организации'


class CreditCompany(models.Model):
    """
    Кредитная ораганизация
    """
    name = models.CharField('Название', max_length=80, null=False, blank=False)

    def __str__(self):
        return '{}//{}'.format(self.pk, self.name)
    
    class Meta:
        verbose_name = 'Кредитная организация'
        verbose_name_plural = 'Кредитные организации'


class Offer(BaseTimestamps):
    """
    Предложение на оформление кредита
    """
    TYPES = (
        (0, 'Потребительское предложение'),
        (1, 'Ипотека'),
        (2, 'Автокредит'),
    )

    # Дата и время
    rotation_start = Timestamp('Начало ротации', null=False, blank=False, default=timezone.now)
    rotation_end = Timestamp('Окончание ротации', null=False, blank=False, default=timezone.now)

    # Прочее
    name = models.CharField('Название', max_length=90, null=False, blank=False, default='')
    offer_type = models.IntegerField('Тип', choices=TYPES, null=False, blank=False, default=0)
    score_min = models.DecimalField('Минимальный скоринговый балл', max_digits=5, decimal_places=2, null=False, blank=False)#Score(verbose_name='Минимальный скоринговый балл',)
    score_max = models.DecimalField('Максимальный скоринговый балл', max_digits=5, decimal_places=2, null=False, blank=False)#Score(verbose_name='Максимальный скоринговый балл',)
    credit_organization = models.ForeignKey(CreditCompany,  null=False, blank=False, on_delete=models.CASCADE, related_name='offer_list', 
        verbose_name='Кредитная организация')

    def __str__(self):
        return '{}//{}'.format(self.pk, self.name)
    
    class Meta:
        verbose_name = 'Кредитное предложение'
        verbose_name_plural = 'Кредитные предложения'


class ClientProfile(BaseTimestamps):
    """
    Анкета клиента
    """

    firstname = models.CharField('Имя', max_length=25, null=False, blank=False)
    surname = models.CharField('Фамилия', max_length=25, null=False, blank=False)
    patronymic = models.CharField('Отчество', max_length=30, null=False, blank=False)
    phone = models.CharField('Номер телефона', max_length=14, null=False, blank=False,)
    passport = models.CharField('Паспорт', max_length=10, null=False, blank=False,)
    score = models.DecimalField('Скоринговый балл', max_digits=5, decimal_places=2, null=False, blank=False)
    partner = models.ForeignKey(User, null=False, blank=False, on_delete=models.CASCADE, related_name='client_profile_list', 
        verbose_name='Партнер')

    def __str__(self):
        return '{}//{}'.format(self.pk, self.surname)
    
    class Meta:
        verbose_name = 'Анкета клиента'
        verbose_name_plural = 'Анкеты клиентов'



class RequestToCС(BaseTimestamps):
    """
    Заявка в кредитную организацию (CreditCompany)
    """

    STATUSES = (
        (0, 'Новая'),
        (1, 'Отправлена'),
        (2, 'Получена'),
        (3, 'Одобрено'),
        (4, 'Отказано'),
        (5, 'Выдано'),
    )

    client_profile = models.ForeignKey(ClientProfile, null=False, blank=False, on_delete=models.CASCADE, related_name='requests_to_cc', 
        verbose_name='Анкета клиента')
    offer = models.ForeignKey(Offer, null=False, blank=False, on_delete=models.CASCADE, related_name='requests_to_cc', 
        verbose_name='Предложения')
    status = models.IntegerField('Статус', choices=STATUSES, null=False, blank=False, default=0)

    cc_to = models.ForeignKey(User, on_delete=models.CASCADE, null=False, blank=False, related_name='requests_to', 
        verbose_name='КО-получатель')

    def __str__(self):
        return '{}//{}'.format(self.pk, self.surname)
    
    class Meta:
        verbose_name = 'Заявка в КО'
        verbose_name_plural = 'Заявки в КО'