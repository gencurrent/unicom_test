from unicom_test import models as unicom
from django.contrib import admin


@admin.register(unicom.Partner)
class PartnerAdmin(admin.ModelAdmin):
    list_display = ['name']


@admin.register(unicom.CreditCompany)
class CreditCompanyAdmin(admin.ModelAdmin):
    list_display = ['name']


@admin.register(unicom.Offer)
class OfferAdmin(admin.ModelAdmin):
    list_display = ['name', 'offer_type',]


@admin.register(unicom.ClientProfile)
class ClientProfileAdmin(admin.ModelAdmin):
    list_display = ['surname', 'firstname', 'patronymic', 'passport', 'score', 'partner']


@admin.register(unicom.RequestToCС)
class RequestToCCAdmin(admin.ModelAdmin):
    list_display = ['client_profile', 'offer', 'status']