from profile import models as profile
from django.contrib.auth.models import User
from django.contrib import admin


class UserInline(admin.StackedInline):
    model = profile.Profile

admin.site.unregister(User)
@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    fields = ['username', 'email', 'first_name']
    inlines = [UserInline]
    search_fields = ['username', 'email', 'first_name', 'last_name']