from django.db import models
from profile.managers import UserManager
from django.contrib.auth.models import User


class Profile(models.Model):
    """
    Собственная модель пользователя
    Прикреплена к модели Django-пользователя
    """
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=False,
        related_name='profile', 
        verbose_name='Пользователь')
    is_partner = models.BooleanField('Является партнером')
    credit_company = models.ForeignKey('unicom_test.CreditCompany', null=True, blank=True, default=None,
        on_delete=models.CASCADE,
        related_name='profiles',
        verbose_name='Принадлежит кредитной организации')
